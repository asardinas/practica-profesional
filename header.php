<?php
session_start();
?>

    <link type="text/css" href="menu.css" rel="stylesheet">
    <script type="text/javascript" src="jquery.js"></script>
    <script type="text/javascript" src="menu.js"></script> 
<div id="menu" style="left: 50px">
    <ul class="menu">
        <li><a href="index.php" class="parent"><span>Home</span></a>
        </li>
        <li><a href="#" class="parent"><span>Ventas</span></a>
            <div><ul>
                <li><a href="#" class="parent"><span>Registrar Venta</span></a>
                    <div><ul>
                        <li><a href="#"><span>0 Km</span></a></li>
                        <li><a href="#"><span>Usado</span></a></li>
                    </ul></div>
                </li>
                <li><a href="#" class="parent"><span>Modificar Venta</span></a>
                    <div><ul>
                        <li><a href="#"><span>0 Km</span></a></li>
                        <li><a href="#"><span>Usado</span></a></li>
                    </ul></div>
                </li>
				<li><a href="#" class="parent"><span>Comprobante Venta</span></a>
                    <div><ul>
                        <li><a href="#"><span>0 Km</span></a></li>
                        <li><a href="#"><span>Usado</span></a></li>
                    </ul></div>
                </li>
                <li><a href="#"><span>Sub Item 3</span></a></li>
                <li><a href="#"><span>Sub Item 4</span></a></li>
            </ul></div>
        </li>
        <li><a href="#"><span>Clientes</span></a>
			<div><ul>
                <li><a href="#" ><span>Agregar Clientes</span></a></li>
                <li><a href="personas.php" ><span>Modificar Clientes</span></a></li>
				
            </ul></div>
        </li>
		<li><a href="#"><span>Service</span></a>
			<div><ul>
                <li><a href="#" ><span>Registrar Service</span></a></li>
                <li><a href="#" ><span>Modificar Service</span></a></li>
				<li><a href="#" class="parent"><span>Tareas</span></a>
                    <div><ul>
                        <li><a href="#"><span>Crear Tarea</span></a></li>
                        <li><a href="#"><span>Modificar Tarea</span></a></li>
                    </ul></div>
                </li>
            </ul></div>
        </li>
		<li><a href="#"><span>Turnos</span></a>
			<div><ul>
                <li><a href="#" ><span>Reservar Turnos</span></a></li>
                <li><a href="#" ><span>Turnos Disponibles</span></a></li>
				<li><a href="#" ><span>Modificar Turno</span></a></li>
            </ul></div>
        </li>
		<li><a href="#"><span>Empleados</span></a>
			<div><ul>
				<li><a href="#" class="parent"><span>Vendedor</span></a>
                    <div><ul>
                        <li><a href="#"><span>Crear Vendedor</span></a></li>
                        <li><a href="#"><span>Modificar Vendedor</span></a></li>
                    </ul></div>
                </li>
				<li><a href="#" class="parent"><span>Mecanico</span></a>
                    <div><ul>
                        <li><a href="#"><span>Crear Mecanico</span></a></li>
                        <li><a href="#"><span>Modificar Mecanico</span></a></li>
                    </ul></div>
                </li>
            </ul></div>
        </li>
        <li class="last"><a href="#"><span>Reportes</span></a>
			<div><ul>
				<li><a href="#" class="parent"><span>Reporte Ventas</span></a>
                    <div><ul>
                        <li><a href="#"><span>0 Km</span></a></li>
                        <li><a href="#"><span>Usado</span></a></li>
                    </ul></div>
                </li>
                <li><a href="#" ><span>Reporte Vendedores</span></a></li>
                <li><a href="#" ><span>Reporte Service</span></a></li>
				<li><a href="#" ><span>Reporte Autos</span></a></li>
            </ul></div>
		</li>
		<li class="last"><a href="#"><span>Seguridad</span></a>
			<div><ul>
				<li><a href="usuario.php" ><span>Usuarios</span></a></li>
                <li><a href="#" ><span>Roles</span></a></li>
                <li><a href="#" ><span>Permisos</span></a></li>
				<li><a href="change_pass.php" ><span>Cambiar Password</span></a></li>
            </ul></div>
		</li>
    </ul>
	<form method="post" action="header.php" >
	<input type="image" src="ButtonLogout.gif" name="logo" value="logout">
	</form>
	<?php
	
if (($_POST["logo"]<>"")){
  
session_start();  

$_SESSION = array();  

session_destroy();  

print "<meta http-equiv=Refresh content=\"0 ; url=login.php\">";

 
} 
	echo ("Hola: ".$_SESSION['user']);
	?>
	
</div>
<div id="copyright" style="display:none" > <a href="http://apycom.com/">Apycom jQuery Menus</a></div>